/* acctools.h */

/* get line into s, return length */
int accgetline(char s[], int lim);

/* find and return index of t in s, or -1 if not found */
int strindex(char s[], char t[]);
