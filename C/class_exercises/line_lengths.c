#include <stdio.h>
#include "../lib/acctools.h"

#define MAXLINE 1000   /* maximum input line size */

/* print the length of each input line */
int main()
{
    int len;                           /* current line length */
    char line[MAXLINE];                /* current input line */
    int linenum = 0;                   /* current line number */

    while ((len = accgetline(line, MAXLINE)) > 0) {
        printf("Line %u: %u\n", ++linenum, len);
    }

    return 0;
}
