#include <stdio.h>
#include "../lib/acctools.h"

int main() 
{
    char *not = "not";
    char *occur = "occur";    

    printf("char '%s' starts at %d in the char '%s'\n", occur, strindex(not, occur), not);
}
