#include <stdio.h>

/* strcmp: return <0 of s<t, 0 if s==t, >0 if s>t */
int strcmp(char *s, char *t) {
    for ( ; *s == *t; s++, t++)
        if (*s == '\0')
            return 0;
        return *s - *t;
}
