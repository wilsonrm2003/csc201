#include <stdio.h>

/* strlen: return legth of string s */
int book_strlen(char *s) {
    int n;

    for (n =0; *s != '\0'; ++s)
        n++;
    return n;
}
