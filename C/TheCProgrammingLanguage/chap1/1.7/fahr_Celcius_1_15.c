#include <stdio.h>

#define LOWER 0 // lower temp limit
#define UPPER 300 // upper limit
#define STEP 20 // step size

int celsius(int fahr);

main() {
    int i; 
    
    for (i = LOWER; i <= UPPER; i = i + STEP)
        printf("%d %d\n", i, celsius(i));
    return 0;
}

int celsius(int fahr) {
    int c;
    
    c = 0; 
    c = (5.0/9.0)*(fahr-32);
    return c;
}
