#include <stdio.h>

#define HIGHCHAR 127 // number of chars before unknown
#define LOWCHAR 32 // lowest character 

int main() {
    int c, i;
    int nChar[HIGHCHAR];

    for (i = 0; i < HIGHCHAR; ++i)
	nChar[i] = 0;
	
    while ((c = getchar()) != EOF){
	++nChar[c];
    }
    
    for (i = LOWCHAR; i < HIGHCHAR; ++i)
	printf("The number of the character '%c' is: %d\n", i, nChar[i]);	
}
