#include <stdio.h>

#define IN 1 // inside a word
#define OUT 0 // outside a word
#define MAXLEN 46 // length of longest word in English

int main(){
    int i, c, wc, wl, state;
    int nLength[MAXLEN];
	
    for (i = 0; i < MAXLEN; ++i)
	nLength[i] = 0; // stopping garbage

    state = OUT;
    wc = wl = 0;
    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t')
            state = OUT;
        else if (state == OUT){
	   wc++; 
	   state = IN;
        }
	if (wc == 1){
	    wl++;
	    if (c == ' ' || c == '\n' || c == '\t'){
		i = wl;
		++nLength[i-1];
		wc = wl = 0;
		state = OUT;
	    }
	}
    }
    for (i = 1; i < MAXLEN; ++i)
	printf("The number of words %d letters long is: %d\n", i, nLength[i]);
}
