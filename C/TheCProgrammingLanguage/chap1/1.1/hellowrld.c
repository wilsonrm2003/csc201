#include <stdio.h> // tells compiler to include info abt the standard input/output library

main(){ // def a func named main
    printf("hello, world\n"); // main calls the libary func printf - \n is a new line 
// printf stops working after you press enter and produces an error 
}
