#include <stdio.h>
#include "../../../lib/acctools.h"

#define MAXLINE 1000 //max char in a line

int main() {
    int len; // current line len
    int max; // max len seen so far
    char line[MAXLINE]; // current input line
    char longest[MAXLINE]; // longest line saved

    max = 0;
    while ((len = accgetline(line, MAXLINE)) > 0)
        if (len > max) {
            max = len;
            copy(longest, line);
        }
    if (max >= 500)
        printf("The longest line is %d characters long. Here is it's text %s\n", max, longest);
    if (max > 0)
        printf("%s\n", longest);
    return 0;
}
