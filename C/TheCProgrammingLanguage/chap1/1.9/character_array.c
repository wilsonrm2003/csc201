#include <stdio.h>

#define MAXLINE 1000 // max input line

int accgetline(char line[], int maxline);
void copy(char to[], char from[]);

//print longest input line
int main() {
    int len; // current line len
    int max; // max len seen so far
    char line[MAXLINE]; // current input line
    char longest[MAXLINE]; // longest line saved

    max = 0;
    while ((len = accgetline(line, MAXLINE)) > 0)
        if (len > max) {
            max = len;
            copy(longest, line);
        }
    if (max > 0)
        printf("%s\n", longest);
    return 0;
}

// get line read a line into s return length
int accgetline(char s[], int lim) {
    int c, i;
    
    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

// copy 'from' to 'to'; assume is big enough
void copy(char to[], char from[]) {
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}
