#include <stdio.h>
#include "../../../lib/acctools.h"

#define MAXLINE 1000 // max char len

int main() {
    int len; // current length
    int i;
    char line[MAXLINE]; // current input line
    
    while((len = accgetline(line, MAXLINE)) > 0) { 
        if (len > 2) {
            for (i = len-1; (line[i-1] == ' ' || line[i-1] == '\t' || line[i-1] == '\n'); i--)
                ;
            line[i++] = '\0';
            printf("%s\n", line);
        }
    }
    return 0;
}
