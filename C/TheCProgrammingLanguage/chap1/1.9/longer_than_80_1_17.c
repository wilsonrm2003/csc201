#include <stdio.h>
#include "../../../lib/acctools.h"

#define MAXLINE 1000 // max char length
#define PRINTLINE 80 // after this number the lines will print 

int main() {
    int len; // current line length
    char line[MAXLINE]; // current input line
    char saved_line[MAXLINE]; // saved line

    while ((len = accgetline(line, MAXLINE)) > 0)
        if (len > PRINTLINE) {
            copy(saved_line, line);
            printf("This line was longer than 80 chars. Here it is again:\n %s\n", saved_line);
        }
    return 0;
}
