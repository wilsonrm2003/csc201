#code from Jelkner
import os
import random
import sys
import time


def main(names_file, topics_file):
    with open(names_file) as f:
        students = [student.strip() for student in f.readlines()]
        course = students.pop(0)
        title = f"\nPresentations for the {course} Course"

    with open(topics_file) as f:
        topics = [topic.strip() for topic in f.readlines()]

    print(title)
    print("=" * (len(title) - 1))

    for topic in topics:
        if not students:
            print(f"{topics} will not be presented")
        std1 = random.choice(students)
        students.remove(std1)
        if topic[0] == "*":
            std2 = random.choice(students)
            students.remove(std2)
            print(f"{std1} and {std2} will present {topic[1:]}.")
        else:
            print(f"{std1} will present {topic}.")
        sys.stdout.flush()
        time.sleep(2)


if __name__ == "__main__":
    if len(sys.argv) != 3 or not os.path.isfile(sys.argv[1]):
        print("Invalid input.")
        print("  Usage: choose_presentations [names_file] [presentations_file]")
        exit()
    main(sys.argv[1], sys.argv[2])

