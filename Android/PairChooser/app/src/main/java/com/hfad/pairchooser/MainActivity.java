package com.hfad.pairchooser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<String> sTudents = new ArrayList<String>(){};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public TextView onClickRemoveStudent(View view){
        EditText enterStudentName = (EditText)findViewById(R.id.enterStudentName);
        TextView students = (TextView)findViewById(R.id.students);
        String studentName = enterStudentName.getText().toString();
        sTudents.remove(studentName.toString());
        String prettyStudents = "";
        int size = sTudents.size();
        for (int i = 0; i < size; i++){
            prettyStudents = (sTudents.get(i) +" \n");
        }
        students.setText(prettyStudents);
        return students;
    }
    public TextView onClickAddStudent(View view){
        EditText enterStudentName = (EditText)findViewById(R.id.enterStudentName);
        String studentName = enterStudentName.getText().toString();
        TextView students = (TextView)findViewById(R.id.students);
        sTudents.add(studentName.toString());
        String prettyStudents = "";
        int size = sTudents.size();
        for (int i = 0; i < size; i++){
            prettyStudents = (sTudents.get(i) +" \n");
        }
        students.setText(prettyStudents);
        return students;
    }
    public TextView onClickChoosePairs(View view) {
        ArrayList<String> pairs1 = new ArrayList<>();
        List<String> halfOfStudents = new ArrayList<>();
        List<String> otherHalfOfStudents = new ArrayList<>();
        int size = sTudents.size();
        TextView pairs;
        pairs = findViewById(R.id.pairs);
        for(int i = 0;  i < size / 2; i++){
            halfOfStudents.add(sTudents.get(i));
        }
        for (int i = size / 2; i < size; i++){
            otherHalfOfStudents.add(sTudents.get(i));
        }
        Collections.shuffle(otherHalfOfStudents);
        Collections.shuffle(halfOfStudents);
        for (int i = 0; i < size / 2; i++){
            pairs1.add(halfOfStudents.get(i) + " is  paired with " + otherHalfOfStudents.get(i));
        }
        TextView students = (TextView)findViewById(R.id.students);
        String prettyStudents = "";
        for (int i = 0; i < size; i++){
            prettyStudents = (sTudents.get(i) +" \n");
        }
        String prettyPairs = "";
        int size2 = pairs1.size();
        for (int i = 0; i < size2; i++){
            prettyPairs = (pairs1.get(i) + "\n");
        }
        students.setText(prettyStudents);
        pairs.setText(prettyPairs);
        return pairs;
    }
}