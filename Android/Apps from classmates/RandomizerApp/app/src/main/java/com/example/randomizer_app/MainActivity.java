package com.example.randomizer_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public TextView onClickRandomPairs(View view) {
        TextView pairs = (TextView) findViewById(R.id.result);
        String name1;
        String name2;
        String finalWord = "";
        ArrayList<String> names = new ArrayList<String>();
        Collections.addAll(names, "Jakob", "Amirah", "Lucas", "Miguel", "Leila",
                "Liam", "Jio", "Rachael");
        Collections.shuffle(names);
        name1 = names.get(0);
        name2 = names.get(1);
        finalWord += name1 + " + " + name2 + " are a pair. ";
        name1 = names.get(2);
        name2 = names.get(3);
        finalWord += name1 + " + " + name2 + " are a pair. ";
        name1 = names.get(4);
        name2 = names.get(5);
        finalWord += name1 + " + " + name2 + " are a pair. ";
        name1 = names.get(6);
        name2 = names.get(7);
        finalWord += name1 + " + " + name2 + " are a pair. ";

        finalWord += "Christopher, Eduardo, and Jack will work together."; // ya'll left yourselves out
        pairs.setText(finalWord);
        return pairs;
    }
}