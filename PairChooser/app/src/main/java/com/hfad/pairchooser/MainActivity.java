package com.hfad.pairchooser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<String> sTudents = new ArrayList<String>(){};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sTudents.add("Rachael");
        sTudents.add("Lucas");
        sTudents.add("Liam");
        sTudents.add("Jio");
        sTudents.add("Miguel");
        sTudents.add("Amirah");
        sTudents.add("Jakob");
        sTudents.add("Leila");
        TextView students = (TextView)findViewById(R.id.students);
        students.setText(sTudents.toString());
    }

    public TextView onClickRemoveStudent(View view){
        EditText enterStudentName = (EditText)findViewById(R.id.enterStudentName);
        TextView students = (TextView)findViewById(R.id.students);
        String studentName = enterStudentName.getText().toString();
        sTudents.remove(studentName.toString());
        students.setText(sTudents.toString());
        return students;
    }
    public TextView onClickAddStudent(View view){
        EditText enterStudentName = (EditText)findViewById(R.id.enterStudentName);
        String studentName = enterStudentName.getText().toString();
        TextView students = (TextView)findViewById(R.id.students);
        sTudents.add(studentName.toString());
        students.setText(sTudents.toString());
        return students;
    }

    public TextView onClickChoosePairs(View view) {
        ArrayList<String> pairs1 = new ArrayList<>();
        List<String> halfOfStudents = new ArrayList<>();
        List<String> otherHalfOfStudents = new ArrayList<>();
        Collections.shuffle(sTudents);
        int size = sTudents.size();
        TextView pairs;
        pairs = findViewById(R.id.pairs);
        if (size % 2 == 0) {
            for (int i = 0; i < size / 2; i++) {
                halfOfStudents.add(sTudents.get(i));
            }
            for (int i = size / 2; i < size; i++) {
                otherHalfOfStudents.add(sTudents.get(i));
            }
            for (int i = 0; i < size / 2; i++) {
                pairs1.add(halfOfStudents.get(i) + " is  paired with " + otherHalfOfStudents.get(i));
            }
        }
        else if (size % 2 != 0) {
            for (int i = 0; i < size / 2; i++) {
                halfOfStudents.add(sTudents.get(i));
            }
            for (int i = size / 2; i < size; i++) {
                otherHalfOfStudents.add(sTudents.get(i));
            }
            for (int i = 0; i < size / 2; i++) {
                pairs1.add(halfOfStudents.get(i) + " is  paired with " + otherHalfOfStudents.get(i));
            }
            pairs1.add(sTudents.get(size - 1) + " is lonely ;( ");
        }
        pairs.setText(pairs1.toString());
        return pairs;
    }
}